//
//  VCMain.swift
//  Extrato
//
//  Created by Lucas Wottrich on 01/06/2018.
//  Copyright © 2018 Lucas Wottrich. All rights reserved.
//

import UIKit

class VCMain: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbAvailable: UILabel!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbExpent: UILabel!
    
    var index: IndexPath?
    var modelData: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Extratos"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        load()
        // Do any additional setup after loading the view.
    }
    
    func load(){
        if modelData != nil {
            lbName?.text = modelData?.name
            lbAvailable?.text = modelData?.limits.available
            lbTotal?.text = modelData?.limits.total
            lbExpent?.text = modelData?.limits.expent
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if let data = self.modelData {
                return data.installments_due.count
            }
        }
        else {
            if let data = self.modelData {
                return data.installments_overdue.count
            }
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "tbCell", for: indexPath) as? ExtractCell else {return UITableViewCell.init()}
        var installment = Data.Installments()
        if indexPath.section == 0 {
            installment = (self.modelData?.installments_due[indexPath.row])!
        } else {
            installment = (self.modelData?.installments_overdue[indexPath.row])!
        }
        cell.lbDate.text = installment.past_due
        cell.lbCarnet.text = installment.carnet
        cell.lbInstallment.text = installment.installment
        cell.lbTotalValue.text = installment.value
        
        if indexPath == index {
            cell.ivSelect.image = UIImage.init(named: "baseline_check_circle_black_24pt")
        } else {
            cell.ivSelect.image = UIImage.init(named: "baseline_check_circle_outline_black_24pt")
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.index = indexPath
        
        var installment = Data.Installments()
        if indexPath.section == 0 {
            installment = (self.modelData?.installments_due[indexPath.row])!
        } else {
            installment = (self.modelData?.installments_overdue[indexPath.row])!
        }
        sendAlert(m: installment.detail.toString())
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func sendAlert(m: String?){
        let alert = UIAlertController(title: "Atenção", message: m, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
