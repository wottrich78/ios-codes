//
//  VCLogin.swift
//  Extrato
//
//  Created by Lucas Wottrich on 01/06/2018.
//  Copyright © 2018 Lucas Wottrich. All rights reserved.
//

import UIKit

class VCLogin: UIViewController {
    
    @IBOutlet weak var txtLogin: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    var modelData: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sMain" {
            guard let vc = segue.destination as? VCMain else {return}
            vc.modelData = modelData
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func onClick(_ sender: Any) {
        //        guard let btn = sender as? UIButton else {return}
        //        switch btn.tag {
        //        case 1:
        //
        //        default:
        //
        //        }
        //        btn.setTitle(txtLogin.text, for: .normal)
        getLogin()
        
    }
    
    func getLogin(){
        if validateTextFild(c1: (txtLogin?.text)!, c2: (txtPassword?.text)!) {
            if isEmpty(txt: txtLogin) && isEmpty(txt: txtPassword){
                self.sendAlert(m: "Campo vazio!")
                return
            } else {
                guard let pwd = txtPassword.text else {return}
                NetworkTest.getData(pwd, success: { (data) in
                    self.modelData = data
                    self.performSegue(withIdentifier: "sMain", sender: self)
                }) { (error) in
                    self.sendAlert(m: error)
                }
            }
        } else {
            self.sendAlert(m: "Campos não coencidem")
        }
    }
    
    func isEmpty(txt: UITextField) -> Bool {
        return (txt.text?.isEmpty)!
    }
    
    func validateTextFild(c1:String, c2: String) -> Bool{
        return c1.elementsEqual(c2)
    }
    
    func sendAlert(m: String?){
        let alert = UIAlertController(title: "Atenção", message: m, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Click", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
