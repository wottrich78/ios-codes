//
//  NetworkTest.swift
//  Extrato
//
//  Created by Lucas Wottrich on 04/06/2018.
//  Copyright © 2018 Lucas Wottrich. All rights reserved.
//

import Foundation
import ObjectMapper

class NetworkTest{
    static var Url: String = "http://www.icoded.com.br/api/extract.json"
    
    class func getData(_ pwd: String, success : @escaping(_ data : Data) -> Void, failure : @escaping(_ error : String) -> Void) {
        
        Url = Url+"?pwd="+pwd
        guard let url = URL(string: Url) else {return}
        
        Network.request(url: url, method: .get){
            (response) in
            guard let statusCode = response.response?.statusCode, statusCode == 200 else {
                failure("Senha incorreta")
                return
            }
            guard let result = response.result.value! as? [String: Any], let json = result["data"] as? [String: Any] else {
                failure("Erro ao logar")
                return
            }
            
            guard let data = Mapper<Data>().map(JSON: (json as! [String: Any])) else {
                failure("Error")
                return
            }
            success(data)
        }
    }
}

